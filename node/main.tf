# ########
# Provider
# ########

provider "proxmox" {
  pm_tls_insecure = var.pm_tls_insecure
  pm_parallel = var.pm_parallel
}


# #########
# terraform
# #########

# rest of the backend part will be filled by terragrunt
terraform {
  backend "s3" {}
}


# ############
# VM Resources
# ############


resource "proxmox_vm_qemu" "this" {
  count = var.vm_count

  name = "${var.name}-0${count.index + 1}${var.fqdn}"
  desc = var.desc

  # Node name has to be the same name as within the cluster
  # this might not include the FQDN
  target_node = var.target_node

  # The destination resource pool for the new VM
  pool = var.pool

  # The template name to clone this vm from
  clone = var.clone
  full_clone = var.full_clone

  # Activate QEMU agent for this VM
  agent = var.agent

  os_type = var.os_type
  cores = var.cores
  sockets = var.sockets
  vcpus = var.vcpus
  cpu = var.cpu
  memory = var.memory
  scsihw = var.scsihw

  lifecycle {
    ignore_changes = [
      bootdisk,
      network,
      disk
    ]
  }

  # Setup the disk. The id has to be unique
  dynamic "disk" {
    for_each = var.disks

    content {
      id = lookup(disk.value, "id", disk.key)
      type = lookup(disk.value, "type", "scsi")
      storage = lookup(disk.value, "storage", "local")
      storage_type = lookup(disk.value, "storage_type", "dir")
      size = lookup(disk.value, "size", "8")
      format = lookup(disk.value, "format", "qcow2")
      cache = lookup(disk.value, "cache", "none")
      backup = lookup(disk.value, "backup", "false")
      iothread = lookup(disk.value, "iothread", "false")
      replicate = lookup(disk.value, "replicate", "false")
      mbps = lookup(disk.value, "mbps", "0")
      mbps_rd = lookup(disk.value, "mbps_rd", "0")
      mbps_rd_max = lookup(disk.value, "mbps_rd_max", "0")
      mbps_wr = lookup(disk.value, "mbps_wr", "0")
      mbps_wr_max = lookup(disk.value, "mbps_wr_max", "0")
    }
  }

  dynamic "network" {
    for_each = var.networks

    content {
      id = lookup(network.value, "id", network.key)
      model = lookup(network.value, "model", "virtio")
      macaddr = network.key == 0  && lookup(var.public_ips[count.index], "macaddr", "") != "nil" ? lookup(var.public_ips[count.index], "macaddr", "") : lookup(network.value, "macaddr", "")
      bridge = lookup(network.value, "bridge", "vmbr0")
      tag = lookup(network.value, "tag", "-1")
      firewall = lookup(network.value, "firewall", "false")
      rate = lookup(network.value, "rate", "-1")
      queues = lookup(network.value, "queues", "-1")
      link_down = lookup(network.value, "link_down", "false")
    }
  }


  # Setup the ip address using cloud-init.
  # Keep in mind to use the CIDR notation for the ip.
  ipconfig0 = lookup(var.public_ips[count.index], "macaddr", "") != "nil" ? lookup(var.public_ips[count.index], "ip", var.ipconfig0) : var.ipconfig0
  ipconfig1 = var.ipconfig1
  ipconfig2 = var.ipconfig2

  sshkeys = var.sshkeys
  nameserver = var.nameserver
}
