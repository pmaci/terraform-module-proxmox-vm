variable "name" {
  description = "(Required) Name of the VM"
  type = string
}

variable "target_node" {
  description = "(Required) Node to place the VM on"
  type = string
}

variable "fqdn" {
  description = "Add FQDN as prefix to name. Start with '.'!"
  type = string
  default = ""
}


variable "desc" {
  description = "(Optional) Description of the VM"
  type = string
  default = ""
}

variable "bios" {
  description = "(Optional; defaults to seabios)"
  type = string
  default = "seabios"
}

variable "onboot" {
  description = "(Optional) Start the vm on boot"
  type = bool
  default = true
}

variable "bootdisk" {
  description = "(Optional); defaults to true"
  type = bool
  default = true
}

variable "agent" {
  description = "(Optional); defaults to 0"
  type = string
  default = "0"
}

variable "iso" {
  description = "(Optional)"
  type = string
  default = ""
}

variable "clone" {
  description = "(Optional) The template name to clone this vm from"
  type = string
  default = ""
}

variable "full_clone" {
  description = "(Optional) Launch vm in full clone mode or not"
  type = bool
  default = true
}

variable "hastate" {
  description = "(Optional)"
  type = string
  default = ""
}

variable "qemu_os" {
  description = "(Optional) defaults to l26"
  type = string
  default = "l26"
}

variable "memory" {
  description = "How much ram will the vm have? In MB"
  type = string
  default = "512"
}

variable "balloon" {
  description = "Optional; defaults to 0"
  type = string
  default = "0"
}

variable "cores" {
  description = "How many cores the will the vm have access to? defaults to 1"
  type = string
  default = "1"
}

variable "sockets" {
  description = "Optional; How many sockets will the vm have access to? defaults to 1"
  type = string
  default = "1"
}

variable "vcpus" {
  description = "Optional; Limit how much virtual cpu can the vm have. Default to 0 (0 Means no limit)"
  type = string
  default = "0"
}

variable "cpu" {
  description = "Optional; Same CPU as the Physical host, possible to add cpu flags. Default to host"
  type = string
  default = "host"
}

variable "numa" {
  description = "Optional; Default to false"
  type = bool
  default = false
}

variable "hotplug" {
  description = "Optional; Default to network,disk,usb"
  type = bool
  default = false
}

variable "scsihw" {
  description = "Optional; defaults to the empty string"
  type = string
  default = "lsi"
}

variable "vga" {
  description = "Optional; vga configuration for the vm"
  type = map(string)
  default = {
    "type" = "std"
    "memory" = "16"
  }
}

variable "networks" {
  description = "Optional; network configuration for the vm"
  type = list(map(string))
  default = [{}]
}

variable "disks" {
  description = "Optional; disk configuration for the vm"
  type = list(map(string))
  default = [{}]
}

variable "serial" {
  description = "Optional"
  type = map(string)
  default = {
    "id" = "0"
    "type" = "socket"
  }
}

variable "pool" {
  description = "Optional; The destination resource pool for the new VM"
  type = string
  default = ""
}

variable "force_create" {
  description = "Optional; Enable / disable force create. Default to false"
  type = bool
  default = false
}

variable "clone_wait" {
  description = "Optional"
  type = string
  default = ""
}

variable "preprovision" {
  description = "Optional; defaults to true"
  type = bool
  default = true
}

variable "os_type" {
  description = "(Optional) Which provisioning method to use, based on the OS type. Possible values: ubuntu, centos, cloud-init. Default to cloud-init"
  type = string
  default = "cloud-init"
}

# The following arguments are specifically for Linux for pre-provisioning.
variable "os_network_config" {
  description = "(Optional) Linux provisioning specific, /etc/network/interfaces for Ubuntu and /etc/sysconfig/network-scripts/ifcfg-eth0 for CentOS."
  type = string
  default = ""
}

variable "ssh_forward_ip" {
  description = "(Optional) Address used to connect to the VM"
  type = string
  default = ""
}

variable "ssh_host" {
  description = "(Optional)"
  type = string
  default = ""
}

variable "ssh_port" {
  description = "(Optional)"
  type = string
  default = ""
}

variable "ssh_user" {
  description = "(Optional)"
  type = string
  default = ""
}

variable "ssh_private_key" {
  description = "(Optional; sensitive) Private key to login in the VM when pre-provisioning."
  type = string
  default = ""
}

# The following arguments are specifically for Cloud-init for pre-provisioning.
variable "ci_wait" {
  description = "(Optional) Cloud-init specific, how to long to wait for pre-provisioning."
  type = string
  default = "600"
}

variable "ciuser" {
  description = "(Optional) Cloud-init specific, overwrite image default user."
  type = string
  default = ""
}

variable "cipassword" {
  description = "(Optional) Cloud-init specific, password to assign to the user."
  type = string
  default = ""
}

variable "cicustom" {
  description = "(Optional) Cloud-init specific, location of the custom cloud-config files."
  type = string
  default = ""
}

variable "searchdomain" {
  description = "(Optional) Cloud-init specific, sets DNS search domains for a container."
  type = string
  default = ""
}

variable "nameserver" {
  description = "(Optional) Cloud-init specific, sets DNS server IP address for a container."
  type = string
  default = ""
}

variable "sshkeys" {
  description = "(Optional) Cloud-init specific, public ssh keys, one per line"
  type = string
}

variable "ipconfig0" {
  description = "(Optional) Cloud-init specific, [gw=] [,gw6=] [,ip=<IPv4Format/CIDR>] [,ip6=<IPv6Format/CIDR>]"
  type = string
  default = "ip=dhcp"
}

variable "ipconfig1" {
  description = "(Optional) Cloud-init specific, [gw=] [,gw6=] [,ip=<IPv4Format/CIDR>] [,ip6=<IPv6Format/CIDR>]"
  type = string
  default = "ip=dhcp"
}

variable "ipconfig2" {
  description = "(Optional) Cloud-init specific, [gw=] [,gw6=] [,ip=<IPv4Format/CIDR>] [,ip6=<IPv6Format/CIDR>]"
  type = string
  default = "ip=dhcp"
}

variable "start" {
  description = "Enable / disable if the vm will start after launch"
  type = bool
  default = true
}

variable "pm_api_url" {
  description = "API endpoint of the Proxmox server. Ex; https://<hostname or ip>:8006/api2/json"
  type = string
  default = ""
}

variable "pm_user" {
  description = "API user's username"
  type = string
  default = "root@pam"
}

variable "pm_password" {
  description = "Password of Proxmox API user"
  type = string
  default = ""
}

variable "pm_tls_insecure" {
  description = "Accept self signed certificate or not"
  type = bool
  default = true
}

variable "pm_parallel" {
  description = "Jobs that can run in parallel. Keep it in 20 because of some bug in Proxmox provisioner"
  type = string
  default = "20"
}

variable "vm_count" {
  description = "How many virtual machines to be launched?"
  type = string
}

variable "public_ips" {
  description = "Optional; list of public IP addresses & virtual MACs"
  type = list(map(string))
  default = [
    {
      ip = null
      macaddr = null
    }
  ]
}
