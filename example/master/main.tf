terraform {
  backend "local" {}
}

module "master-test-001" {
  source = "../../master"

  vm_count = "1"
  name = "test-001-master"
  fqdn = ".internal.korcoin.net"
  desc = ""
  clone = "deb10-tmpl"
  full_clone = "true"
  cores = "2"
  sockets = "1"
  vcpus = "2"

  # list of disk in map. id must be unique
  disks = [
    {
      size = "30"
    }
  ]

  # list of network cards in map. id must be unique
  networks = [
    {
      id = "0"
      bridge = "vmbr1"
    },
    {
      id = "1"
      bridge = "vmbr1"
    }
  ]

  private_ips = [
    {
      ip = "nil"
    },
    {
      ip = "nil"
    },
    {
      ip = "nil"
    },
    {
      ip = "nil"
    }
  ]

  // these variables must be set nil if no need for public_ip
  public_ips = [
    {
      ip = "nil"
      macaddr = "nil"
    },
    {
      ip = "nil"
      macaddr = "nil"
    },
    {
      ip = "nil"
      macaddr = "nil"
    }
  ]

  # [gw=] [,gw6=] [,ip=<IPv4Format/CIDR>] [,ip6=<IPv6Format/CIDR>]
  # nic id 0
  ipconfig0 = "ip=dhcp"
  # nic id 1
  ipconfig1 = "ip=dhcp"
  # nic id 2
  ipconfig2 = "ip=dhcp"

  memory = "2048"
  # this set to internal dnsmasq server
  nameserver = "10.128.0.1"
  sshkeys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTCkoz0CkN92ew4iJCMPsvRrBmiDEwtrZWt7tPAxH0gFoC350+X0IFt3BTmuK1B7s65GuDL+IBu1K2QwWzHpZO82ARWgcy9STf209fX4iSs2npJFVa76fdpA6CtSzOsOz58i8xdxnlKqePGOx3/XKrnosyQzhwZX8YbdxT09i0Ocn0zAl8ikJfspSMxNB4+dZv/JZNy8sZBROH7PkvQiiDPA1jhI9qMkowISrePqTYN3QOCPEAqV9imOQ32m/PlfFf14+OG+YlFIOxngI5JuDUlWgKaPOh6g/dp7DgXJ/fJva9dBY9cNHqVUXpqH3Qjgqdcnn6tX7eXYBJ9fYzm4yf"
  target_node = "proxmox001"
}
