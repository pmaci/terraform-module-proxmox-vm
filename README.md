# terraform-module-proxmox-vm

terraform module to create virtual machine in Proxmox

# requirements:
- [Telmate/terraform-provider-proxmox](https://github.com/Telmate/terraform-provider-proxmox)
